package com.example.kinomaniak.avatar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import android.widget.TextView;
import android.widget.Toast;
import com.example.kinomaniak.R;

import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;

public class AvatarActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_REQUEST_CODE = 108;
    private static final String KEY = "602da28b";
    private ImageView imgAvatar;
    private Button btnDownload;
    private Button btnLoad;
    private Button btnCapture;
    private TextView txtMovieName;
    Intent dataIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        btnLoad = (Button) findViewById(R.id.btnLoad);
        btnCapture = (Button) findViewById(R.id.btnCapture);

        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        txtMovieName = (TextView) findViewById(R.id.txtMovieName);

        btnDownload.setOnClickListener(this);
        btnLoad.setOnClickListener(this);
        btnCapture.setOnClickListener(this);

        dataIntent = getIntent();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);


        if(savedInstanceState != null) {
            String title = savedInstanceState.getString("title");
            txtMovieName.setText(title);
            Bitmap tmp = savedInstanceState.getParcelable("picture");
            if(tmp != null) {
               imgAvatar.setImageBitmap(tmp);
            }
        }
        else {

            //DEFAULT POSTER
            String movieName = "It";
            String posterLink = downloadPoster(movieName, KEY);
            Log.i("INFO", "Dane: " + posterLink);
            if (posterLink != "") {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(posterLink, imgAvatar);
                imgAvatar.setTag(imgAvatar.getDrawable());
            } else {
                Toast toast = Toast.makeText(this, "Sprawdź dostęp do internetu", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 650);
                toast.show();
                this.finish();
            }
        }
    }

    @Override
    public void onClick(View v) {

        if (btnDownload.getId() == v.getId()) {

            String movieName = txtMovieName.getText().toString();
            String posterLink = downloadPoster(movieName, KEY);
            if (posterLink != "") {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(posterLink, imgAvatar);
                imgAvatar.setTag(imgAvatar.getDrawable());
            }
        }

        if (btnLoad.getId() == v.getId()) {

            BitmapDrawable drawable = (BitmapDrawable) imgAvatar.getDrawable();
            Bitmap bitmap = drawable.getBitmap();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            dataIntent.putExtra("picture", byteArray);
            setResult(Activity.RESULT_OK, dataIntent);
            finish();
        }

        if (btnCapture.getId() == v.getId()) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        }

    }

    public String downloadPoster(String movieName, String KEY) {
        OmdbClient omdbClient = new OmdbClient();
        String jsonResponse = omdbClient.searchMovieByTitle(movieName, KEY);
        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);
            String posterLink = jsonObject.get("Poster").toString();
            return posterLink;
        } catch (JSONException JSONe) {
            JSONe.printStackTrace();
            return "";
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imgAvatar.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onSaveInstanceState( Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
        if(imgAvatar.getDrawable() != null)
        {
            BitmapDrawable drawable = (BitmapDrawable) imgAvatar.getDrawable();
            if(drawable.getBitmap() != null){
                Bitmap bitmap = drawable.getBitmap();
                outState.putParcelable("picture",bitmap);
                outState.putString("title",txtMovieName.getText().toString());
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}