package com.example.kinomaniak.avatar;

import android.os.StrictMode;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class OmdbClient {

    public static String sendGetRequest(String requestUrl) {
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
            }
            bufferedReader.close();
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }


    public static String searchMovieByTitle(String title, String apikey) {
        final String SEARCH_MOVIE_URL = "https://www.omdbapi.com/?t=TITLE&apikey=APIKEY";
        String requestUrl = SEARCH_MOVIE_URL
                .replace("TITLE", title)
                .replace("APIKEY", apikey)
                .replace(" ", "+");

        return sendGetRequest(requestUrl);
    }
}