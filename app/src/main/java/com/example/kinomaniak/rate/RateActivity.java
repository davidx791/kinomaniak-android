package com.example.kinomaniak.rate;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.kinomaniak.avatar.AvatarActivity;
import com.example.kinomaniak.movies.DatabaseHelperMovies;
import com.example.kinomaniak.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RateActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_PERMISSIONS_AVATAR =  102;
    String[] permissionsAvatar = {Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

    private Button btnCalculate;
    private Button btnSave;
    private Button btnClear;
    private Button btnBack;
    private Button btnAvatar;

    private TextView txtMovieName;
    private TextView txtScore;
    private ImageView imgAvatar;
    private RatingBar rtngB;
    private RadioGroup radioG1;
    private RadioGroup radioG2;
    private RadioGroup radioG3;
    private RadioGroup radioG4;
    private RadioButton rb1_1;
    private RadioButton rb1_2;
    private RadioButton rb1_3;
    private RadioButton rb2_1;
    private RadioButton rb2_2;
    private RadioButton rb2_3;
    private RadioButton rb3_1;
    private RadioButton rb3_2;
    private RadioButton rb3_3;
    private RadioButton rb4_1;
    private RadioButton rb4_2;
    private RadioButton rb4_3;
    private SeekBar seekB1;
    private SeekBar seekB2;
    private CheckBox checkB1;
    private CheckBox checkB2;
    private boolean avatarChosen;
    DatabaseHelperMovies databaseHelperMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        databaseHelperMovies = new DatabaseHelperMovies(this);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnAvatar = (Button) findViewById(R.id.btnAvatar);

        txtMovieName = (TextView)findViewById(R.id.txtMovieName);
        txtScore = (TextView)findViewById(R.id.txtScore);
        imgAvatar = (ImageView)findViewById(R.id.imgAvatar);

        rtngB = (RatingBar)findViewById(R.id.rtngB);
        radioG1 = (RadioGroup)findViewById(R.id.radioG1);
        radioG2 = (RadioGroup)findViewById(R.id.radioG2);
        radioG3 = (RadioGroup)findViewById(R.id.radioG3);
        radioG4 = (RadioGroup)findViewById(R.id.radioG4);
        seekB1 = (SeekBar)findViewById(R.id.seekB1);
        seekB2 = (SeekBar)findViewById(R.id.seekB2);
        checkB1 = (CheckBox)findViewById(R.id.checkB1);
        checkB2 = (CheckBox)findViewById(R.id.checkB2);

        rb1_1 = (RadioButton)findViewById(R.id.rb1_1);
        rb1_2 = (RadioButton)findViewById(R.id.rb1_2);
        rb1_3 = (RadioButton)findViewById(R.id.rb1_3);

        rb2_1 = (RadioButton)findViewById(R.id.rb2_1);
        rb2_2 = (RadioButton)findViewById(R.id.rb2_2);
        rb2_3 = (RadioButton)findViewById(R.id.rb2_3);

        rb3_1 = (RadioButton)findViewById(R.id.rb3_1);
        rb3_2 = (RadioButton)findViewById(R.id.rb3_2);
        rb3_3 = (RadioButton)findViewById(R.id.rb3_3);

        rb4_1 = (RadioButton)findViewById(R.id.rb4_1);
        rb4_2 = (RadioButton)findViewById(R.id.rb4_2);
        rb4_3 = (RadioButton)findViewById(R.id.rb4_3);

        btnCalculate.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnAvatar.setOnClickListener(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        if(savedInstanceState != null) {
            String title = savedInstanceState.getString("title");
            float rtng = savedInstanceState.getFloat("ratingBar");
            String score = savedInstanceState.getString("score");
            boolean rBut1_1 = savedInstanceState.getBoolean("rb1_1");
            boolean rBut1_2 = savedInstanceState.getBoolean("rb1_2");
            boolean rBut1_3 = savedInstanceState.getBoolean("rb1_3");
            boolean rBut2_1 = savedInstanceState.getBoolean("rb2_1");
            boolean rBut2_2 = savedInstanceState.getBoolean("rb2_2");
            boolean rBut2_3 = savedInstanceState.getBoolean("rb2_3");
            boolean rBut3_1 = savedInstanceState.getBoolean("rb3_1");
            boolean rBut3_2 = savedInstanceState.getBoolean("rb3_2");
            boolean rBut3_3 = savedInstanceState.getBoolean("rb3_3");
            boolean rBut4_1 = savedInstanceState.getBoolean("rb4_1");
            boolean rBut4_2 = savedInstanceState.getBoolean("rb4_2");
            boolean rBut4_3 = savedInstanceState.getBoolean("rb4_3");
            int sb1 = savedInstanceState.getInt("seekB1");
            int sb2 = savedInstanceState.getInt("seekB2");
            boolean cB1 = savedInstanceState.getBoolean("checkB1");
            boolean cB2 = savedInstanceState.getBoolean("checkB2");

            txtMovieName.setText(title);
            txtScore.setText(score);
            rtngB.setRating(rtng);
            if(rBut1_1)        rb1_1.setChecked(true);
            else{  if(rBut1_2) rb1_2.setChecked(true);
                else {         rb1_3.setChecked(true); }}
            if(rBut2_1)        rb2_1.setChecked(true);
            else{  if(rBut2_2) rb2_2.setChecked(true);
            else {             rb2_3.setChecked(true); }}
            if(rBut3_1)        rb3_1.setChecked(true);
            else{  if(rBut3_2) rb3_2.setChecked(true);
            else {             rb3_3.setChecked(true); }}
            if(rBut4_1)        rb4_1.setChecked(true);
            else{  if(rBut4_2) rb4_2.setChecked(true);
            else {             rb4_3.setChecked(true); }}
            seekB1.setProgress(sb1);
            seekB2.setProgress(sb2);
            checkB1.setChecked(cB1);
            checkB2.setChecked(cB2);

            Bitmap tmp = savedInstanceState.getParcelable("picture");
            if(tmp != null) {
                avatarChosen = true;
                btnAvatar.setVisibility(View.GONE);
                imgAvatar.setImageBitmap(tmp);
                imgAvatar.setVisibility(View.VISIBLE);
            }
        }
        else {
            avatarChosen = false;
            imgAvatar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(imgAvatar.getId() == v.getId() || btnAvatar.getId() == v.getId()){
            if (checkPermissionAvatar()) {
                Intent chooseAvatarIntent = new Intent(RateActivity.this,AvatarActivity.class);
                startActivityForResult(chooseAvatarIntent,107);
            }
        }

        if(btnCalculate.getId() == v.getId()){

            double part1 = 0, score = 0;
            int part2 = 0, part3 = 0, part4 = 0, part5 = 0, part6 = 0, part7 = 0, part8 = 0, part9 = 0;

            part1 = rtngB.getRating();

            if (radioG1.getCheckedRadioButtonId() == rb1_2.getId()) { part2 = 1; }
            if (radioG1.getCheckedRadioButtonId() == rb1_3.getId()) { part2 = 2; }
            if (radioG2.getCheckedRadioButtonId() == rb2_2.getId()) { part3 = 1; }
            if (radioG2.getCheckedRadioButtonId() == rb2_3.getId()) { part3 = 2; }
            if (radioG3.getCheckedRadioButtonId() == rb3_2.getId()) { part4 = 1; }
            if (radioG3.getCheckedRadioButtonId() == rb3_3.getId()) { part4 = 2; }
            if (radioG4.getCheckedRadioButtonId() == rb4_2.getId()) { part5 = 1; }
            if (radioG4.getCheckedRadioButtonId() == rb4_3.getId()) { part5 = 2; }

            part6 = seekB1.getProgress();
            part7 = seekB2.getProgress();

            if (checkB1.isChecked()) { part8 = 1;}
            if (checkB2.isChecked()) { part9 = 1;}

            score = (part1 + part2 + part3 + part4 + part5 + part6 + part7 + part8 + part9)/2;
            int scoreInt = (int) Math.round(score);
            txtScore.setText(String.valueOf(scoreInt));
        }

        if(btnSave.getId() == v.getId()){
            if(txtMovieName.length()>0 && txtScore.length()>0 && avatarChosen)
            {
                String title = txtMovieName.getText().toString();
                int score = Integer.valueOf(txtScore.getText().toString());
                byte[] avatar = convertDrawable2ByteArray((BitmapDrawable) imgAvatar.getDrawable());

                boolean insertData = databaseHelperMovies.addData(title, score, avatar);
                if (insertData){
                    Toast.makeText(this,"Dodano do bazy",Toast.LENGTH_SHORT).show();
                    btnClear.performClick();
                }
                else{
                    Toast.makeText(this,"Błąd zapisu",Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                String message ="";
                if (txtMovieName.length()== 0){ message += "Wpisz tytuł. "; }
                if (txtScore.length()== 0)    { message += "Wylicz ocenę. ";    }
                if (!avatarChosen)            { message += "Ustaw awatar.";}
                Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
            }
        }

        if(btnClear.getId() == v.getId()){
            txtScore.setText("");
            txtMovieName.setText("");
            rtngB.setRating(0);
            rb1_1.setChecked(true);
            rb2_1.setChecked(true);
            rb3_1.setChecked(true);
            rb4_1.setChecked(true);
            seekB1.setProgress(0);
            seekB2.setProgress(0);
            checkB1.setChecked(false);
            checkB2.setChecked(false);
            avatarChosen = false;
            imgAvatar.setVisibility(View.GONE);
            btnAvatar.setVisibility(View.VISIBLE);
        }

        if(btnBack.getId() == v.getId()){
            this.finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if (requestCode == 107 && resultCode == Activity.RESULT_OK)
            {
                avatarChosen = true;
                btnAvatar.setVisibility(View.GONE);

                byte[] byteArray = data.getByteArrayExtra("picture");
                Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imgAvatar.setImageBitmap(bitmap);
                imgAvatar.setVisibility(View.VISIBLE);
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public byte[] convertDrawable2ByteArray(BitmapDrawable drawable){
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public boolean checkPermissionAvatar (){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String i : permissionsAvatar)
        {
            if(ContextCompat.checkSelfPermission(this, i) != PackageManager.PERMISSION_GRANTED)
            {
                listPermissionsNeeded.add(i);
            }
        }

        if(!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_PERMISSIONS_AVATAR);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS_AVATAR: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }

    @Override
    public void onSaveInstanceState( Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
        if(imgAvatar.getDrawable() != null)
        {
            BitmapDrawable drawable = (BitmapDrawable) imgAvatar.getDrawable();
            if(drawable.getBitmap() != null){
                Bitmap bitmap = drawable.getBitmap();
                outState.putParcelable("picture",bitmap);
            }
        }
        outState.putString("title",txtMovieName.getText().toString());
        outState.putFloat("ratingBar",rtngB.getRating());
        outState.putString("score",txtScore.getText().toString());
        outState.putBoolean("rb1_1",rb1_1.isChecked());
        outState.putBoolean("rb1_2",rb1_2.isChecked());
        outState.putBoolean("rb1_3",rb1_3.isChecked());
        outState.putBoolean("rb2_1",rb2_1.isChecked());
        outState.putBoolean("rb2_2",rb2_2.isChecked());
        outState.putBoolean("rb2_3",rb2_3.isChecked());
        outState.putBoolean("rb3_1",rb3_1.isChecked());
        outState.putBoolean("rb3_2",rb3_2.isChecked());
        outState.putBoolean("rb3_3",rb3_3.isChecked());
        outState.putBoolean("rb4_1",rb4_1.isChecked());
        outState.putBoolean("rb4_2",rb4_2.isChecked());
        outState.putBoolean("rb4_3",rb4_3.isChecked());
        outState.putInt("seekB1",seekB1.getProgress());
        outState.putInt("seekB2",seekB2.getProgress());
        outState.putBoolean("checkB1",checkB1.isChecked());
        outState.putBoolean("checkB2",checkB2.isChecked());
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


}