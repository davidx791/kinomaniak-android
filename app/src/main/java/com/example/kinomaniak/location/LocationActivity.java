package com.example.kinomaniak.location;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.kinomaniak.R;
import com.example.kinomaniak.cinemas.Cinema;
import com.example.kinomaniak.cinemas.CinemasActivity;
import com.example.kinomaniak.cinemas.DatabaseHelperCinemas;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class LocationActivity extends AppCompatActivity implements LocationListener, View.OnClickListener, OnMapReadyCallback {

    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
    private TextView txtMyLongitude;
    private TextView txtMyLatitude;
    private TextView txtNearestCinema;
    private Button btnCinemaViewBase;
    private Button btnCinemaSearch;
    private Button btnCinemaNavigate;
    private Button btnMyLocation;

    private double myLongitudeDouble;
    private double myLatitudeDouble;
    private double nearestLongitudeDouble;
    private double nearestLatitudeDouble;

    SupportMapFragment supportMapFragment;
    DatabaseHelperCinemas databaseHelperCinemas;
    GoogleMap gMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        btnCinemaViewBase = (Button) findViewById(R.id.btnCinemaViewBase);
        btnCinemaSearch = (Button) findViewById(R.id.btnCinemaSearch);
        btnMyLocation = (Button) findViewById(R.id.btnMyLocation);
        btnCinemaNavigate = (Button) findViewById(R.id.btnCinemaNavigate);

        txtMyLongitude = (TextView) findViewById(R.id.txtMyLongitude);
        txtMyLatitude = (TextView) findViewById(R.id.txtMyLatitude);
        txtNearestCinema = (TextView) findViewById(R.id.txtNearestCinema);

        databaseHelperCinemas = new DatabaseHelperCinemas(this);
        String cinemaData = loadData("cinemaBase.txt"); //baza kin
        String[] cinemaDataArray = cinemaData.split("\n");

        if (databaseHelperCinemas.getRowNumber() != cinemaDataArray.length) { //uzupelnij bazę kin
            databaseHelperCinemas.deleteAll();
            databaseHelperCinemas.updateId();
            for (int i = 0; i < cinemaDataArray.length; i++) {
                String[] elements = cinemaDataArray[i].split("\\,");
                databaseHelperCinemas.addData(elements[0], elements[2], elements[1], "0");
            }
        }

        btnCinemaViewBase.setOnClickListener(this);
        btnCinemaSearch.setOnClickListener(this);
        btnMyLocation.setOnClickListener(this);
        btnCinemaNavigate.setOnClickListener(this);

        if (ContextCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED
               && ContextCompat.checkSelfPermission(this, permissions[1]) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3, 0, this);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location !=null) {
                myLatitudeDouble = location.getLatitude();
                myLongitudeDouble = location.getLongitude();
                setLocationInTextView(myLongitudeDouble, myLatitudeDouble);
            }

        }
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        supportMapFragment.getMapAsync(LocationActivity.this);

        if(savedInstanceState != null) {
            String nearestCinema = savedInstanceState.getString("cinema");
            txtNearestCinema.setText(nearestCinema);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        LatLng latLng = new LatLng(Double.valueOf(txtMyLatitude.getText().toString()),Double.valueOf(txtMyLongitude.getText().toString()));
        Log.i("INFO",latLng.latitude+" "+latLng.longitude);
        MarkerOptions markerOptions = new MarkerOptions().position((latLng)).title("Here");
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,12));
        googleMap.addMarker(markerOptions);
    }

    @Override
    public void onClick(View v) {
        if (btnCinemaViewBase.getId() == v.getId()) {
            ArrayList<Cinema> dataSerial = refreshCinemaBase();
            Intent intent = new Intent(LocationActivity.this, CinemasActivity.class);
            intent.putExtra("data", dataSerial);
            startActivity(intent);
        }

        if (btnCinemaSearch.getId() == v.getId()) {
            Cinema nearestCinema = searchNearestCinema();
            nearestLongitudeDouble = Double.valueOf(nearestCinema.getLongitude());
            nearestLatitudeDouble = Double.valueOf(nearestCinema.getLatitude());
            txtNearestCinema.setText(nearestCinema.getName());

            LatLng nearestLatLng = new LatLng(nearestLatitudeDouble,nearestLongitudeDouble);
            MarkerOptions markerOptions = new MarkerOptions().position((nearestLatLng)).title(txtNearestCinema.getText().toString());
            gMap.animateCamera(CameraUpdateFactory.newLatLng(nearestLatLng));
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nearestLatLng,12));
            gMap.addMarker(markerOptions);
        }

        if (btnCinemaNavigate.getId() == v.getId()) {
            String startLat = txtMyLatitude.getText().toString();
            String startLong = txtMyLongitude.getText().toString();
            String stopLat = String.valueOf(nearestLatitudeDouble);
            String stopLong = String.valueOf(nearestLongitudeDouble);
            if(nearestLatitudeDouble > 0 &&nearestLongitudeDouble >0) {
                Intent navigateIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
                        "http://maps.google.com/maps?" + "saddr=" + startLat + "," + startLong + "&" + "daddr=" + stopLat + "," + stopLong));
                startActivity(navigateIntent);
            }
            else{
                Toast.makeText(this, "Najpierw wyszukaj najbliższe kino", Toast.LENGTH_SHORT).show();
            }
        }

        if (btnMyLocation.getId() == v.getId()) {
            gMap.clear();
            LatLng latLng = new LatLng(Double.valueOf(txtMyLatitude.getText().toString()),Double.valueOf(txtMyLongitude.getText().toString()));
            MarkerOptions markerOptions = new MarkerOptions().position((latLng)).title("Me");
            gMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,12));
            gMap.addMarker(markerOptions);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        myLatitudeDouble = location.getLatitude();
        myLongitudeDouble = location.getLongitude();
        setLocationInTextView(myLongitudeDouble,myLatitudeDouble);
    }

    public void setLocationInTextView(Double myLong, Double myLat){
        myLong = round(myLong,3);
        myLat = round(myLat,3);

        txtMyLongitude.setText(myLong.toString());
        txtMyLatitude.setText(myLat.toString());
    }

    public String loadData(String inputFile) {
        String tContents = "";
        try {
            InputStream stream = getAssets().open(inputFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {}

        return tContents;
    }

    public double calculateDistance(double long1, double lat1, double long2, double lat2) {
        long1 = Math.toRadians(long1);
        long2 = Math.toRadians(long2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double dlon = long2 - long1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2);

        double c = 2 * Math.asin(Math.sqrt(a));
        double dist = 6371 * c; // 3956 or 6371

        return dist;
    }

    private ArrayList<Cinema> refreshCinemaBase() {
        Cursor data = databaseHelperCinemas.getData();
        ArrayList<Cinema> dataList = new ArrayList<>();

        while (data.moveToNext()) {
            double myLongitude, myLatitude, longitudeTmp, latitudeTmp;

            myLongitude = Double.valueOf(txtMyLongitude.getText().toString());
            myLatitude = Double.valueOf(txtMyLatitude.getText().toString());
            longitudeTmp = Double.valueOf(data.getString(2));
            latitudeTmp = Double.valueOf(data.getString(3));
            double dist = calculateDistance(myLongitude, myLatitude, longitudeTmp, latitudeTmp);

            String itemId = " " + data.getString(0) + ".";
            String itemName = " " + data.getString(1);
            String itemLong = " " + data.getString(2);
            String itemLat = " " + data.getString(3);
            String itemDist = " " + String.valueOf(dist).substring(0, 5);

            Cinema cinema = new Cinema(itemId, itemName, itemLong, itemLat, itemDist);
            dataList.add(cinema);
        }
        return dataList;
    }

    private Cinema searchNearestCinema() {
        ArrayList<Cinema> data = refreshCinemaBase();
        Collections.sort(data);
        for (Cinema cinema : data) {
            Log.i("Location", "data: " + cinema.getId() + " " + cinema.getDist());
        }

        return data.get(0);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onSaveInstanceState( Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
        outState.putString("cinema",txtNearestCinema.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

}
