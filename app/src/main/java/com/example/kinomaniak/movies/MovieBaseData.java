package com.example.kinomaniak.movies;

public class MovieBaseData {

    String id;
    String title;
    String score;
    byte[] avatar;

    public MovieBaseData( String id, String title, String score, byte[] avatar ) {

        this.id=id;
        this.title=title;
        this.score=score;
        this.avatar = avatar;
    }
}