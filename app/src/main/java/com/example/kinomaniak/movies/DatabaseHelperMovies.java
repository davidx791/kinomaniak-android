package com.example.kinomaniak.movies;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelperMovies extends SQLiteOpenHelper {

    private static final String TABLE_NAME = "Filmy";
    private static final String COL1 = "Nr";
    private static final String COL2 = "Tytuł_filmu";
    private static final String COL3 = "Ocena";
    private static final String COL4 = "Miniatura";

    public DatabaseHelperMovies(Context context){
        super(context,TABLE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " ("+ COL1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL2 + " TEXT, "+ COL3 + " INTEGER,  "+ COL4 + " BLOB);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }

    public boolean addData(String itemTitle, int itemScore, byte[] itemAvatar){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL2,itemTitle);
        contentValues.put(COL3,itemScore);
        contentValues.put(COL4, itemAvatar);

        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1){ return false; }
        else            { return true;  }
    }

    public Cursor getData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLE_NAME;
        Cursor data = db.rawQuery(query,null);

        return data;
    }

    public Cursor getScoreData(int n)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+COL3+ " = "+n;
        Cursor data = db.rawQuery(query,null);

        return data;
    }

    public Cursor getTitleData(String title)
    {
        Log.i("INFO",title);
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+COL2+ " = "+"'"+title+"'";
        Cursor data = db.rawQuery(query,null);

        return data;
    }

    public int deleteById(int deleteId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = COL1 + " = " + deleteId;

        return db.delete(TABLE_NAME,query,null);
    }

    public int deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_NAME,null,null);
    }

    public void updateId()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
    }

}