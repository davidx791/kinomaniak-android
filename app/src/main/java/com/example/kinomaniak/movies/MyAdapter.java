package com.example.kinomaniak.movies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.kinomaniak.R;
import java.util.List;

public class MyAdapter extends ArrayAdapter<MovieBaseData> {

    private int layoutResource;

    public MyAdapter(Context context, int layoutResource, List<MovieBaseData> movieBaseDataList) {
        super(context, layoutResource, movieBaseDataList);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        MovieBaseData movieBaseData = getItem(position);

        if (movieBaseData != null) {

            TextView left = (TextView) view.findViewById(R.id.leftTxtView);
            TextView center = (TextView) view.findViewById(R.id.centerTxtView);
            TextView right = (TextView) view.findViewById(R.id.rightTxtView);
            ImageView poster = (ImageView) view.findViewById(R.id.imgView);

            Log.i("INFO", "MyAdapter: " + poster);
            if (poster != null) {
                byte[] byteArray = movieBaseData.avatar;
                Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                poster.setImageBitmap(bitmap);
            }
            if (left != null) {
                left.setText(movieBaseData.id);
            }
            if (center != null) {
                center.setText(movieBaseData.title);
            }
            if (right != null) {
                right.setText(movieBaseData.score);
            }
        }

        return view;
    }
}