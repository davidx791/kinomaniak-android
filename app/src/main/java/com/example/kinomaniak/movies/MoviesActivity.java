package com.example.kinomaniak.movies;

import androidx.appcompat.app.AppCompatActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.kinomaniak.R;
import java.util.ArrayList;

public class MoviesActivity extends AppCompatActivity implements View.OnClickListener {

    DatabaseHelperMovies databaseHelperMovies;
    private ListView movieBase;
    private Button btnDeleteId;
    private Button btnDeleteAll;
    private Button btnSearchTitle;
    private Button btnSearchScores;
    private Button btnViewAll;

    private TextView txtId;
    private TextView txtScores;
    private TextView txtTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        movieBase = (ListView)findViewById(R.id.movieBase);
        databaseHelperMovies = new DatabaseHelperMovies(this);

        btnDeleteId = (Button) findViewById(R.id.btnDeleteId);
        btnDeleteAll = (Button) findViewById(R.id.btnDeleteAll);
        btnSearchScores = (Button) findViewById(R.id.btnSearchScores);
        btnSearchTitle = (Button) findViewById(R.id.btnSearchTitle);
        btnViewAll = (Button) findViewById(R.id.btnViewAll);
        txtId = (TextView) findViewById(R.id.txtId);
        txtScores = (TextView) findViewById(R.id.txtScores);
        txtTitle = (TextView) findViewById(R.id.txtTitle);

        btnSearchScores.setOnClickListener(this);
        btnSearchTitle.setOnClickListener(this);
        btnDeleteId.setOnClickListener(this);
        btnDeleteAll.setOnClickListener(this);
        btnViewAll.setOnClickListener(this);

        populateListView(getAllData());
    }

    @Override
    public void onClick(View v) {
        if(btnSearchScores.getId() == v.getId()) {
            if(txtScores.length() >0) {
                populateListView(getScoreData(Integer.valueOf(txtScores.getText().toString())));
            }
            else
            {
                Toast.makeText(this,"Najpierw wprowadź ocenę",Toast.LENGTH_SHORT).show();
            }
        }

        if(btnSearchTitle.getId() == v.getId()) {
            if(txtTitle.length() >0) {
                populateListView(getTitleData(txtTitle.getText().toString()));
            }
            else
            {
                Toast.makeText(this,"Najpierw wprowadź tytuł",Toast.LENGTH_SHORT).show();
            }
        }

        if(btnDeleteId.getId() == v.getId()) {
            if(txtId.length() >0) {
                delById(Integer.valueOf(txtId.getText().toString()));
                populateListView(getAllData());
            }
            else
            {
                Toast.makeText(this,"Najpierw wprowadź ID filmu",Toast.LENGTH_SHORT).show();
            }
        }
        if(btnDeleteAll.getId() == v.getId()) {
            delAll();
            populateListView(getAllData());
            Toast.makeText(this,"Twoja baza ocenionych filmów została usunięta",Toast.LENGTH_SHORT).show();
        }

        if(btnViewAll.getId() == v.getId()) {
            populateListView(getAllData());
        }

    }

    private Cursor getAllData() {
        return databaseHelperMovies.getData();
    }

    private Cursor getTitleData(String title) {
        return databaseHelperMovies.getTitleData(title);
    }

    private Cursor getScoreData(int n) {
        return databaseHelperMovies.getScoreData(n);
    }

    private void populateListView(Cursor data)
    {
        ArrayList<MovieBaseData> dataList = new ArrayList<>();

        while(data.moveToNext())
        {
            String itemId = " "+data.getString(0)+".";
            String itemTitle = " "+data.getString(1);
            String itemScore = " "+data.getString(2);
            byte[] itemAvatar =  data.getBlob(3);
            MovieBaseData movieBaseData = new MovieBaseData(itemId,itemTitle,itemScore, itemAvatar);
            dataList.add(movieBaseData);
        }

        MyAdapter myAdapter = new MyAdapter(this,R.layout.movies_layout,dataList);
        movieBase.setAdapter(myAdapter);
    }

    private void delById(int deleteId)
    {
        databaseHelperMovies.deleteById(deleteId);
        databaseHelperMovies.updateId();
    }

    private void delAll()
    {
        databaseHelperMovies.deleteAll();
        databaseHelperMovies.updateId();
    }

}
