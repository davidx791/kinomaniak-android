package com.example.kinomaniak.cinemas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.kinomaniak.R;
import java.util.List;

public class CinemaAdapter  extends ArrayAdapter<Cinema> {

    private int layoutResource;

    public CinemaAdapter(Context context, int layoutResource, List<Cinema> cinemaList) {
        super(context, layoutResource, cinemaList);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        Cinema cinema = getItem(position);

        if (cinema != null) {
            TextView id = (TextView) view.findViewById(R.id.txtCinemaId);
            TextView name = (TextView) view.findViewById(R.id.txtCinemaName);
            TextView distance = (TextView) view.findViewById(R.id.txtCinemaDist);

            if (id != null) {
                id.setText(cinema.getId());
            }
            if (name != null) {
                name.setText(cinema.getName());
            }
            if (distance != null) {
                distance.setText(String.format("%.2f", Double.valueOf(cinema.getDist())));
            }
        }

        return view;
    }

}

