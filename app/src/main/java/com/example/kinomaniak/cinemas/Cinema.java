package com.example.kinomaniak.cinemas;

import java.io.Serializable;

public class Cinema implements Serializable, Comparable<Cinema> {

    private String id;
    private String name;
    private String longitude;
    private String latitude;
    private String dist;

    public Cinema(String id, String name, String longitude, String latitude, String dist)
    {
        this.id=id;
        this.name=name;
        this.longitude=longitude;
        this.latitude=latitude;
        this.dist=dist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    private double getDoubleDist ()
    {
        return Double.valueOf(dist);
    }

    @Override
    public int compareTo(Cinema cinema) {
        return (this.getDoubleDist() < cinema.getDoubleDist() ? -1 :
                (this.getDoubleDist() == cinema.getDoubleDist() ? 0 : 1));
    }
}

