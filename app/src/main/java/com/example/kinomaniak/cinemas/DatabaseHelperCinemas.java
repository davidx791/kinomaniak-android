package com.example.kinomaniak.cinemas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelperCinemas extends SQLiteOpenHelper {

    private static final String TABLE_NAME = "Cinemas";
    private static final String COL2 = "cinemaName";
    private static final String COL3 = "longitude";
    private static final String COL4 = "latitude";
    private static final String COL5 = "distance";

    public DatabaseHelperCinemas(Context context){
        super(context,TABLE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, "+ COL2 +" TEXT, "+ COL3 +" TEXT, "+ COL4 +" TEXT, "+ COL5+ " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME );
        onCreate(db);
    }

    public boolean addData(String itemName, String itemLong, String itemLat, String itemDist)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2,itemName);
        contentValues.put(COL3,itemLong);
        contentValues.put(COL4,itemLat);
        contentValues.put(COL5,itemDist);
        long result = db.insert(TABLE_NAME,null,contentValues);

        if(result == -1){ return false; }
        else            { return true;  }
    }

    public Cursor getData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;

        return db.rawQuery(query,null);
    }

    public int deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_NAME,null,null);
    }

    public void updateId()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME + "'");
    }

    public long getRowNumber() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        db.close();

        return count;
    }
}
