package com.example.kinomaniak.cinemas;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.example.kinomaniak.R;
import java.util.ArrayList;
import java.util.List;

public class CinemasActivity extends AppCompatActivity implements View.OnClickListener{

    private ListView cinemaBase;
    private Button btnCinemaBaseBack;
    private DatabaseHelperCinemas databaseHelperCinemas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinemas);

        cinemaBase = (ListView) findViewById(R.id.cinemaBase);
        btnCinemaBaseBack = (Button) findViewById(R.id.btnCinemaBaseBack);

        btnCinemaBaseBack.setOnClickListener(this);
        databaseHelperCinemas = new DatabaseHelperCinemas(this);

        ArrayList<Cinema> dataList;
        dataList = (ArrayList<Cinema>) getIntent().getSerializableExtra("data");
        populateListView(dataList);
    }

    @Override
    public void onClick(View v) {
        if (btnCinemaBaseBack.getId() == v.getId()) {
            this.finish();
        }
    }

    private void populateListView(List<Cinema> dataList)
    {
        CinemaAdapter cinemaAdapter = new CinemaAdapter(this, R.layout.cinema_layout, dataList);
        cinemaBase.setAdapter(cinemaAdapter);
    }

}

