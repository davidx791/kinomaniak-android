package com.example.kinomaniak.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.kinomaniak.R;
import com.example.kinomaniak.location.LocationActivity;
import com.example.kinomaniak.movies.MoviesActivity;
import com.example.kinomaniak.rate.RateActivity;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_PERMISSIONS_LOCATION = 101;
    String[] permissionsLocation = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

    private Button btnRateActivity;
    private Button btnMoviesActivity;
    private Button btnLocationActivity;
    private Button btnSettings;
    private Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRateActivity = (Button)findViewById(R.id.btnRateActivity);
        btnMoviesActivity = (Button)findViewById(R.id.btnMoviesActivity);
        btnLocationActivity = (Button)findViewById(R.id.btnLocationActivity);
        btnSettings = (Button)findViewById(R.id.btnSettings);
        btnExit = (Button)findViewById(R.id.btnExit);

        btnRateActivity.setOnClickListener(this);
        btnMoviesActivity.setOnClickListener(this);
        btnLocationActivity.setOnClickListener(this);
        btnSettings.setOnClickListener(this);
        btnExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(btnRateActivity.getId() == v.getId()){
            Intent rateActivityIntent = new Intent(MainActivity.this, RateActivity.class);
            startActivity(rateActivityIntent);
        }

        if(btnMoviesActivity.getId() == v.getId()){
            Intent moviesActivityIntent = new Intent(MainActivity.this, MoviesActivity.class);
            startActivity(moviesActivityIntent);
        }

        if(btnLocationActivity.getId() == v.getId()){
            if (checkPermissionLocation()) {
                Intent locationActivityIntent = new Intent(MainActivity.this, LocationActivity.class);
                startActivity(locationActivityIntent);
            }
        }

        if(btnSettings.getId() == v.getId()){
            Toast.makeText(this,"W przygotowaniu",Toast.LENGTH_SHORT).show();
        }

        if(btnExit.getId() == v.getId()){
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public boolean checkPermissionLocation (){
       List<String> listPermissionsNeeded = new ArrayList<>();
        for(String i : permissionsLocation)
        {
            if(ContextCompat.checkSelfPermission(this, i) != PackageManager.PERMISSION_GRANTED)
            {
                listPermissionsNeeded.add(i);
            }
        }

        if(!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_PERMISSIONS_LOCATION);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }

}
